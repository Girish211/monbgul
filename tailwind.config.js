module.exports = {
  mode: "jit",
  purge: ["./app/**/*.{ts,tsx,js,jsx}"],
  darkMode: "media", // or 'media' or 'class'
  theme: {
    screens: {
      'largeMob': '414px',
      'tab': '768px',
      'laptop': '1024px',
      'desktop': '1280px'
    },
    extend: {
      colors: {
        title: '#ce3939',
        body: '#170b0c',
        bgGrey: '#dfdfdf',
        boxFirst: '#d34f32',
        boxSecond: '#90211d',
      },
      borderRadius: {
        'circle': '88px',
        'half-circle': '48px',
        'services': '20px',
      },
    },
    fontFamily: {
      'Duepuntozero': ['DuepuntozeroRegular'],
      'TerminalDosis-Bold': ['TerminalDosis-Bold'],
      'TerminalDosis-Medium': ['TerminalDosis-Medium'],
      'TerminalDosisRegular': ['TerminalDosisRegular'],
      'TerminalDosis-SemiBold': ['TerminalDosis-SemiBold'],
    },
    backgroundImage: theme => ({
      'banner-pattern': "url('~/assets/svg/bgBanner.svg')",
      'card-dark-red': "url('~/assets/svg/bgCardDarkRed.svg')",
      'card-light-red': "url('~/assets/svg/bgCardLightRed.svg')",
      'card-black-red-square': "url('~/assets/svg/bgCardBlackRedSquare.svg')",
      'card-black-red-rectangle': "url('~/assets/svg/bgCardBlackRedRectangle.svg')",
      'web-application': "url('~/assets/svg/webApplication.svg')",
      'red-dots-circle-art': "url('~/assets/svg/redDotsCircleArt.svg')",
      'red-box': "url('~/assets/svg/redBox.svg')",
      'red-tile': "url('~/assets/svg/redTileBg.svg')",
      'red-tile-no-asp-ratio': "url('~/assets/svg/redTileBgNoAspRation.svg')",
      'orange-tile': "url('~/assets/svg/orangeTileBg.svg')",
      'lenin': "url('~/assets/images/Lenin.jpg')",
    }),
  },
  variants: {},
  plugins: [
    require('@tailwindcss/aspect-ratio'),
    require('@tailwindcss/line-clamp'),
  ]
};